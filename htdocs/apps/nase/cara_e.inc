<b>Program name:</b> Cara<br>
<b>Description:</b> Animated bouncing line.<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/cara.zip">http://ncic.netmag.cz/download/cara.zip</a>  (<!--#fsize virtual="/download/cara.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/cara.zip" --><BR>
<B>Runs as:</b> Java 1.0 Applet<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>