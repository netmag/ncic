<b>Jmeno programu:</b> Kecafon<br>
<b>Popis:</b> Applet, ktery vas vita na nasi homepage. 
Zdrojovy kod je k dispozici a tak si muzete v pripade zajmu dodelat dalsi efekty.
<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/kecafon.html">http://ncic.netmag.cz/apps/nase/kecafon.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="kecafon.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/kecafon.zip">http://ncic.netmag.cz/download/kecafon.zip</a>  (<!--#fsize virtual="/download/kecafon.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/kecafon.zip" --><BR>
<B>Typ:</b> Java 1.0 Applet<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Open Source software pod GNU Public License v2<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>