<b>Program name:</b> Web Bench<br>
<b>Description:</b> Simple benchmarking utility for testing speed of Web Servers.<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/webbench.zip">http://ncic.netmag.cz/download/webbench.zip</a>  (<!--#fsize virtual="/download/webbench.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/webbench.zip" --><BR>
<B>Runs as:</b> Any OS which has C compiler<BR>
<B>License:</b> Open Source Software, GPL v2<BR>
<B>Source code:</b> included<BR>