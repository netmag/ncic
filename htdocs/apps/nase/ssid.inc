<b>Jmeno programu:</b> Server Side Includes Daemon<br>
<b>Popis:</b> Vsichni webmasteri pouzivaji Server Side Includes dokumenty, coz
jim sice usnadnuje praci, ale navstevnici je za to proklinaji, protoze
to maji zbytecne pomale. Tento program vam umozni pouzivat SSI a zaroven potesit vase navstevniky.
Program je take pouzit ke generaci obsahu tohoto serveru.<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/ssid.html">http://ncic.netmag.cz/apps/nase/ssid.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="ssid.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/ssid.zip">http://ncic.netmag.cz/download/ssid.zip</a>  (<!--#fsize virtual="/download/ssid.zip"-->)<BR>
<B>Posledni verze k downloadu:</b> z <!--#flastmod virtual="/download/ssid.zip" --><BR>
<B>Typ:</b> Java 1.1 Applikace<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Freeware<BR>
<B>Zdrojovy kod:</b> Prilozen. Neni vsak obsazen v demo verzi.<BR>