<b>Jmeno programu:</b> NetMag Logo<br>
<b>Popis:</b> Jednoduchy aplet, slouzici jako logo casopisu NetMag. Upozornuji,
ze ten jeho podivny konec neni chyba, ale umysl.<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/netmaglogo.html">http://ncic.netmag.cz/apps/nase/netmaglogo.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="/apps/nase/netmaglogo.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/netmaglogo.zip">http://ncic.netmag.cz/download/netmaglogo.zip</a> (<!--#fsize virtual="/download/netmaglogo.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/netmaglogo.zip" --><BR>
<B>Typ:</b> Java 1.0 Applet<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Free software, GPL licence.<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>