<b>Jmeno programu:</b> System Info<br>
<b>Popis:</b> Applet zobrazi hodnoty <I>system properties</I>, ktere mu povoli
precist Security Manager.
Pokud je spusten
jako aplikace, tak diky neomezenemu pristupu k systemovym udajum,
vypise jmena a hodnoty vsech <I>system properties</I>. Vhodne pro kontrolovani jak
prisne je nastaven security manager u jednotlivych browseru a jake informace
muze applet od vaseho browseru ziskat. Hodi se tez ke kontrolovani verze javy,
kterou vas browser obsahuje.<br>
<B>Autor:</b> puvodni neznamy, upravy <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/systeminfo.html">http://ncic.netmag.cz/apps/nase/systeminfo.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="systeminfo.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/systeminfo.zip">http://ncic.netmag.cz/download/systeminfo.zip</a>  (<!--#fsize virtual="/download/systeminfo.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/systeminfo.zip" --><BR>
<B>Typ:</b> Applet nebo Aplikace<BR>
<B>Zdroj:</b> Modifikovany public domain kod<BR>
<B>Sireno jako:</b> Public domain<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>