<b>Jmeno programu:</b> Cookie Server<br>
<b>Popis:</b> Cookie server - posila klientum nahodne vybrane citaty. Pokud jste jeste
nevideli zadny multi-threaded server v Jave, muzete se seznamit s timto.<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="/apps/nase/cookie.html">http://ncic.netmag.cz/apps/nase/cookie.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="cookie.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/cookie.zip">http://ncic.netmag.cz/download/cookie.zip</a>  (<!--#fsize virtual="/download/cookie.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/cookie.zip" --><BR>
<B>Typ:</b> Java 1.0 Aplikace<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Free Software, GPL licence<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>