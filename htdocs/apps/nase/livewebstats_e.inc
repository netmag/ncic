<b>Program name:</b> Live Web Stats<br>
<b>Description:</b> On line live web access counter. Unlike many others solutions Server part is 
also included.<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/livestats.zip">http://ncic.netmag.cz/download/livestats.zip</a>  (<!--#fsize virtual="/download/livestats.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/livestats.zip" --><BR>
<B>Runs as:</b> Java 1.0 Applet and Application<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>