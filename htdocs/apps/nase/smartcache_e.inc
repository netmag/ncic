<B>Program name:</b> Smart Cache<br>
<b>Description:</b> Full featured proxy-cache server with offline
browsing support and many special features. Ideal replacement for
internal browser cache.<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/smartcache_e.html">http://ncic.netmag.cz/apps/nase/smartcache_e.html</a><BR>
<B>Last updated:</b> <!--#flastmod virtual="smartcache_e.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/scache.zip">http://ncic.netmag.cz/download/scache.zip</a> (<!--#fsize virtual="/download/scache.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/scache.zip" --><BR>
<B>Runs as:</b> Java 1.1 Application<BR>
<B>License:</b> GPL version 2<BR>
<B>Source:</b> included in distribution archive<BR>