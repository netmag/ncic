<B>Jmeno programu:</b> Smart Cache<br>
<b>Popis:</b> Plnohodnotny proxy server s podporou offline browsingu a s mnoha
specialnich vymozenosti. Ackoliv byl puvodne zamyslen jako nahrada za
cache browseru, lze jej take pouzit jako forwarder ci transparent proxy.<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/smartcache.html">http://ncic.netmag.cz/apps/nase/smartcache.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="smartcache.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/scache.zip">http://ncic.netmag.cz/download/scache.zip</a> (<!--#fsize virtual="/download/scache.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/scache.zip" --><BR>
<B>Typ:</b> Applikace<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Free software pod GPL licenci verze 2<BR>
<B>Zdrojovy kod:</b> Obsazen v archivu<BR>