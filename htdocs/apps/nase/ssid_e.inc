<b>Program name:</b> Server Side Includes Daemon<br>
<b>Description:</b> Daemon from generating .html files from .shtml using standard SSI syntax.
Used for generating this site.<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/ssid.zip">http://ncic.netmag.cz/download/ssid.zip</a>  (<!--#fsize virtual="/download/ssid.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/ssid.zip" --><BR>
<B>Runs as:</b> Java 1.1 Application<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>