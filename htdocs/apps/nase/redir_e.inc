<b>Program name:</b> Redir<br>
<b>Description:</b> Utility applet for auto-redirecting visitors to different
locations, which depends on version of their browser.<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/redir.zip">http://ncic.netmag.cz/download/redir.zip</a>  (<!--#fsize virtual="/download/redir.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/redir.zip" --><BR>
<B>Runs as:</b> Java 1.0 Applet<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>