<b>Jmeno programu:</b> Live Web Stats<br>
<b>Popis:</b> On-line pocitadlo pristupu k web strance. Pocitani poctu pristupu za urcita
casova obdobi (hodina, den, tyden, mesic, rok). Na rozdil od klasickych
pocitadel se tento stav meni s tim, jak na vasi stranku pristupuji jednotlivi
uzivatele.<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/livewebstats.html">http://ncic.netmag.cz/apps/nase/livewebstats.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="livewebstats.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/livestats.zip">http://ncic.netmag.cz/download/livestats.zip</a>  (<!--#fsize virtual="/download/livestats.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/livestats.zip" --><BR>
<B>Typ:</b> Java 1.0 Applet a Aplikace<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Open Source, GNU Public v2 license<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>