<b>Jmeno programu:</b> Redir<br>
<b>Popis:</b> Applet na presmerovani browseru jinam v zavislosti na hodnotach systemovych
polozek (browser, verze javy, ...). Nepodminene presmerovani je pochopitelne
podporovano take. Proc ho pouzit? Muzete mit treba jine java a non-java stranky,
nebo stanky optimalizovane pro urcity prohlizec, nebo mate dve verze appletu
v a poskytujete je uzivateli podle toho, jakou ma Javu (1.0 ci 1.1).<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/redir.html">http://ncic.netmag.cz/apps/nase/redir.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="redir.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/redir.zip">http://ncic.netmag.cz/download/redir.zip</a>  (<!--#fsize virtual="/download/redir.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/redir.zip" --><BR>
<B>Typ:</b> Java 1.0 Applet<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Open Source GNU Public license v2.<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>