<B>Jmeno programu:</b> Bouncing Lines<br>
<b>Popis:</b> Jednoducha animace z usecek. Az to shlednete, tak poznate, ze je to
prave ten velmi znamy screensaverovy efekt. A nevypada spatne.<br>
<B>Autor:</b> John Bokma, upravy <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/bouncinglines.html">http://ncic.netmag.cz/apps/nase/bouncinglines.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="bouncinglines.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/bouncinglines.zip">http://ncic.netmag.cz/download/bouncinglines.zip</a>  (<!--#fsize virtual="/download/bouncinglines.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/bouncinglines.zip" --><BR>
<B>Typ:</b> Java 1.0 Applet<BR>
<B>Zdroj:</b> Modifikovany public domain kod<BR>
<B>Sireno jako:</b> Public domain<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>