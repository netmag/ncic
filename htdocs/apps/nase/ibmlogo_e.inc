<b>Program name:</b> Simple IBM Logo<br>
<b>Description:</b> Applet which displays simple IBM logo<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/ibmlogo.zip">http://ncic.netmag.cz/download/ibmlogo.zip</a>  (<!--#fsize virtual="/download/ibmlogo.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/ibmlogo.zip" --><BR>
<B>Runs as:</b> Java 1.0 Applet<BR>
<B>License:</b> Public domain<BR>
<B>Source code:</b> included<BR>