<B>Jmeno programu:</b> IBM LOGO<br>
<b>Popis:</b> Prave jste si stahli JDK a zda se vam, ze hello world je moc primitivni
a chystate se neco napsat? Nic neni tak jednoduche, jak se zda na prvni pohled.<br>
<B>Autor:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> <a href="http://ncic.netmag.cz/apps/nase/ibmlogo.html">http://ncic.netmag.cz/apps/nase/ibmlogo.html</a><BR>
<B>Posledni aktualizace homepage:</b> <!--#flastmod virtual="ibmlogo.shtml" --><BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/ibmlogo.zip">http://ncic.netmag.cz/download/ibmlogo.zip</a>  (<!--#fsize virtual="/download/ibmlogo.zip"-->)<BR>
<B>Posledni verze k downloadu je z:</b> <!--#flastmod virtual="/download/ibmlogo.zip" --><BR>
<B>Typ:</b> Applet<BR>
<B>Zdroj:</b> Puvodni tvorba<BR>
<B>Sireno jako:</b> Public domain<BR>
<B>Zdrojovy kod:</b> Prilozen<BR>