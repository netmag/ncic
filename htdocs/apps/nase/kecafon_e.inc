<b>Program name:</b> Kecafon<br>
<b>Description:</b> Applet for displaying news on the web site with some changing efects<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/kecafon.zip">http://ncic.netmag.cz/download/kecafon.zip</a>  (<!--#fsize virtual="/download/kecafon.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/kecafon.zip" --><BR>
<B>Runs as:</b> Java 1.0 Applet<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>