<b>Program name:</b> NetMag Logo<br>
<b>Description:</b> Java version of NetMag logo<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/netmaglogo.zip">http://ncic.netmag.cz/download/netmaglogo.zip</a>  (<!--#fsize virtual="/download/netmaglogo.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/netmaglogo.zip" --><BR>
<B>Runs as:</b> Java 1.0 Applet<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>