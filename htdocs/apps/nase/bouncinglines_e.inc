<b>Program name:</b> Bouncing Lines<br>
<b>Description:</b> Animated bouncing lines; well known screensaver efect.<br>
<B>Author:</b> John Bokma, hacked by <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/bouncinglines.zip">http://ncic.netmag.cz/download/bouncinglines.zip</a>  (<!--#fsize virtual="/download/bouncinglines.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/bouncinglines.zip" --><BR>
<B>Runs as:</b> Java 1.0 Applet<BR>
<B>License:</b> Public domain<BR>
<B>Source code:</b> included<BR>