<b>Program name:</b> Cookie server<br>
<b>Description:</b> Multi threaded server serving internet "quote of the day" protocol.<br>
<B>Author:</b> <a href="mailto:hsn@cybermail.net">Radim Kolar</a><BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/cookie.zip">http://ncic.netmag.cz/download/cookie.zip</a>  (<!--#fsize virtual="/download/cookie.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/cara.zip" --><BR>
<B>Runs as:</b> Java 1.0 Application<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>