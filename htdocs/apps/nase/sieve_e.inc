<b>Program name:</b> Sieve benchmark<br>
<b>Description:</b> Java version of famous benchmark.<br>
<B>Author:</b> ? <BR>
<B>Homepage:</b> N/A<br>
<B>Last Modified:</b> N/A<BR>
<B>Download:</b> <a href="http://ncic.netmag.cz/download/sieve.zip">http://ncic.netmag.cz/download/sieve.zip</a>  (<!--#fsize virtual="/download/sieve.zip"-->)<BR>
<B>Download file date:</b> <!--#flastmod virtual="/download/sieve.zip" --><BR>
<B>Runs as:</b> Java 1.0 Application<BR>
<B>License:</b> Open Source Software, GPL license<BR>
<B>Source code:</b> included<BR>