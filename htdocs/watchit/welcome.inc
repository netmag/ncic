<LI>URL mus� za��nat <b>http://</b>
<LI>Pokud tu jste poprv�, bude v�m automaticky zalo�eno nov� konto.
Heslo, ktere nap��ete <B>NEBUDE</b> pou�ito jako p��stupove heslo k nov�
zalo�en�mu kontu. 
<LI>Pokud si chcete nastavit u sv�ho konta heslo, pou�ijte
<a href="changepassword.html">tuto str�nku</a>.
<LI>Pokud zad�te URL, ktere je�t� nem� n� syst�m v datab�zi, bude n�sledovat
jeho on-line ov��en� na spr�vnost. Toto ov��en� m��e b�t v
z�vislosti na zat��en� s�t� i pon�kud zdlouhav� operace. Toto ov��ov�n� nem��ete
nijak urychlit (nap�. proveden�m reload). Naopak, pokud stopnete browser d��ve,
ne� bude p��slu�n� str�nka ov��ena, tak se V�m nep�id� do va�eho seznamu
sledovan�ch str�nek.
<LI>Pokud chcete testovat v�mi nov� pridanou str�nku na zm�ny �ast�ji ne� jednou
za den, pou�ijte <a href="pageinfo.html">tento formul��</a>.